const UBER_CAR = "uberCar";
const UBER_SUV = "uberSUV";
const UBER_BLACK = "uberBlack";
function tinhGiaTienKmDauTien(car) {
  if (car == UBER_CAR) {
    return 8000;
  }
  if (car == UBER_SUV) {
    return 9000;
  }
  if (car == UBER_BLACK) {
    return 10000;
  }
}
function tinhGiaTienKm1_19(car) {
  switch (car) {
    case UBER_CAR: {
      return 7500;
    }
    case UBER_SUV: {
      return 8500;
    }
    case UBER_BLACK: {
      return 9500;
    }
    default:
      return 0;
  }
}
function tinhGiaTienKm19TroDi(car) {
    switch (car) {
      case UBER_CAR: {
        return 7000;
      }
      case UBER_SUV: {
        return 8000;
      }
      case UBER_BLACK: {
        return 9000;
      }
      default:
        return 0;
    }
  }

// MAIN FUNCTION
function tinhTienUber() {
  // console.log("yes");
  
  var carOption = document.querySelector('input[name="selector"]:checked').value;
  console.log("Car Option: ", carOption);
  var giaTienKmDauTien = tinhGiaTienKmDauTien(carOption);
  console.log("giaTienKmDauTien: ", giaTienKmDauTien);
  var giaTienKm1_19 = tinhGiaTienKm1_19(carOption);
  console.log("giaTienKm1_19: ",giaTienKm1_19);
  var giaTienKm19TroDi = tinhGiaTienKm19TroDi(carOption);
  console.log("giaTienKm19TroDi: ",giaTienKm19TroDi);

  var soKmDiDuoc = document.getElementById("txt-km").value*1;
  console.log('soKmDiDuoc: ', soKmDiDuoc);

  var tienTra = 0;

  if (soKmDiDuoc <= 1){
    tienTra = giaTienKmDauTien * soKmDiDuoc;
    console.log('tienTra: ', tienTra);
    document.getElementById("xuatTien").innerHTML =` ${tienTra} VND`;
  } else if ( 1 < soKmDiDuoc && soKmDiDuoc <= 19 ){
    tienTra = giaTienKmDauTien + (soKmDiDuoc -1) * giaTienKm1_19;
    console.log('tienTra: ', tienTra);
    document.getElementById("xuatTien").innerHTML =` ${tienTra} VND`;

  } else {
    tienTra = giaTienKmDauTien + 18 * giaTienKm1_19 + ( soKmDiDuoc - 19) * giaTienKm19TroDi;
    console.log('tienTra: ', tienTra);
    document.getElementById("xuatTien").innerHTML =` ${tienTra} VND`;

  }
  // if else (1 < soKmDiDuoc && soKmDiDuoc <= 19) {

  //   tienTra = giaTienKmDauTien + (soKmDiDuoc-1) * giaTienKm1_19;
  //   } 
  // else (soKmDiDuoc > 19) {
  //   tienTra = giaTienKmDauTien + 18 * tinhGiaTienKm1_19 + (soKmDiDuoc - 19)*giaTienKm19TroDi;
  //   }
  // console.log('tienTra: ', tienTra);
}

